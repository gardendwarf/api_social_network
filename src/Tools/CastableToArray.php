<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 18/12/18
 * Time: 09:17
 */

namespace App\Tools;


trait CastableToArray
{
    public function toArray()
    {
        return get_object_vars($this);
    }
}