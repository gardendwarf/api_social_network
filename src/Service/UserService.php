<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/12/18
 * Time: 21:33
 */

namespace App\Service;



use App\Entity\Friendship;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends ApiClient
{

    const POST_USER_ROUTE='api_users_post_collection';
    const GET_USERS_ROUTE='api_users_get_collection';
    const GET_USER_ROUTE='api_users_get_item';
    const PUT_USER_ROUTE='api_users_put_item';
    const DEL_USER_ROUTE='api_users_delete_item';

    protected $keys = ['username', 'password', 'firstName', 'name', 'isActive', 'createdAt', 'updatedAt'];

    /**
     * @param SessionInterface $session
     * @param array $params
     * @return array|bool|string
     */
    public function getUser(SessionInterface $session, array $params = [])
    {

        if (isset($params['id'])) {
            $responseArray = json_decode($this->get(self::GET_USER_ROUTE, $session->get('token'), $params), 1);
            $response = new User();
            $response->setObjectFromArray($responseArray);
        } elseif (isset($params['username'])) {
            $response = $this->manageResponse(
                $this->get(self::GET_USERS_ROUTE, $session->get('token'), $params),
                User::class
            );

            return $response[0];
        } else {
            $response = $this->manageResponse(
                $this->get(self::GET_USERS_ROUTE, $session->get('token'), $params),
                User::class
            );
        }

        return $response;
    }

    /**
     * @param SessionInterface $session
     * @param $params
     * @return bool|string
     */
    public function subscribeUser(sessionInterface $session, $params)
    {
        //var_dump($this->router->generate('register', [], RouterInterface::ABSOLUTE_URL));
        $params['createdAt'] = $this->generateDateString();
        $params['updatedAt'] = $this->generateDateString();
        $ch = curl_init($this->router->generate('register', [], RouterInterface::ABSOLUTE_URL));
        $ch = $this->preparePostCurlOptions($ch, $this->filterParams($params));
        $response = curl_exec($ch);


        curl_close($ch);

        return $response;
    }

    /**
     * @param User|null $user
     * @return array
     */
    public function mergeAllMyFriends(User $user = null) {
        if($user instanceof User) {

            $friends = [];

            foreach ($user->getFriends() as $item) {
                $f = new Friendship();
                $f->setObjectFromArray($item);
                $friends[] = $f->getFriend();
            }
            foreach ($user->getFriendsWithMe() as $friendWithMe) {
                $f = new Friendship();
                $f->setObjectFromArray($friendWithMe);
                $friends[] = $f->getUser();
            }

            $user->setAllMyFriends($friends);

            return $friends;
        }
    }

    /**
     * @param SessionInterface $session
     * @param array $params
     * @return array
     */
    public function getUserList(SessionInterface $session, $params = [])
    {
        $userList = [];
        $liste = json_decode($this->get(UserService::GET_USERS_ROUTE, $session->get('token'), []), 1);
        if($users = $liste['hydra:member']) {
            foreach ($users as $user) {
                $usr = new User();
                $usr->setObjectFromArray($user);
                $userList[] = $usr;
            }
        }

        return $userList;
    }
    /**
     * @param $params
     * @return string
     */
    protected function filterParams($params)
    {
        $request = '';

        foreach ($params as $key=>$param) {
            if(in_array($key, $this->keys) ) {
                $request .= $key . '=' . $param . '&';
            }
        }

        return rtrim($request, '&');
    }
}