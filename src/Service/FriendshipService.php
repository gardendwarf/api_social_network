<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/12/18
 * Time: 21:04
 */

namespace App\Service;


use App\Entity\Friendship;
use App\Entity\User;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

class FriendshipService extends ApiClient
{
    const POST_FRIENDSHIP_ROUTE='api_friendships_post_collection';
    const GET_FRIENDSHIPS_ROUTE='api_friendships_get_collection';
    const GET_FRIENDSHIP_ROUTE='api_friendships_get_item';
    const PUT_FRIENDSHIP_ROUTE='api_friendships_put_item';
    const DEL_FRIENDSHIP_ROUTE='api_friendships_delete_item';

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param SessionInterface $session
     * @param array $params
     * @return array|bool
     */
    public function createFriendship(SessionInterface $session, array $params = [])
    {
        return $this->manageResponse(
            $this->post(self::POST_FRIENDSHIP_ROUTE, $session->get('token'),$params),
            User::class
        );
    }

    public function createFriendshipFromForm(Request $request, $formName, array $additionalValues = [])
    {
        if(!$request->request->get($formName)) {
            return false;
        }

        $post = $request->request->get($formName);
        //die(var_dump($post));
        if (isset($post['user'])) {
            $post['user'] = $this->router->generate(UserService::GET_USER_ROUTE, ['id' => $post['user']]);
        }
        if (isset($post['friend'])) {
            $post['friend'] = $this->router->generate(UserService::GET_USER_ROUTE, ['id' => $post['friend']]);
        }

        return array_merge($post, $additionalValues);
    }

    /**
     * @param SessionInterface $session
     * @param User $u1
     * @param User $u2
     * @return bool
     */
    public function isFriendship(SessionInterface $session, User $u1, User $u2)
    {
        $jwtToken = $session->get('token');
        // /api/friendships/user=$u1->getId();friend=$u2->getId()
        $reponse = $this->get(self::GET_FRIENDSHIP_ROUTE, $jwtToken, ['id' => 'user='.$u1->getId().';friend='.$u2->getId()]);
        $friendship = new Friendship();
        $friendship->setObjectFromArray(json_decode($reponse, 1));
        if ($friendship instanceof Friendship) {
            return true;
        }

        // /api/friendships/user=$u2->getId();friend=$u1->getId()
        $reponse = $this->get(self::GET_FRIENDSHIP_ROUTE, $jwtToken, ['id' => 'user='.$u2->getId().';friend='.$u1->getId()]);
        $friendship = new Friendship();
        $friendship->setObjectFromArray(json_decode($reponse, 1));
        if ($friendship instanceof Friendship) {
            return true;
        }

        return false;
    }

}