<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/12/18
 * Time: 12:14
 */

namespace App\Service;



use Symfony\Component\Routing\RouterInterface;
use App\Entity\User;
use App\Entity\Message;
use App\Entity\Friendship;


class ApiClient
{
    const API_URL = 'http://localhost';

    /**
     * @var RouterInterface
     */
    protected $router;


    /**
     * @param array $data
     * @return bool|string
     */
    public function getToken(array $data = [])
    {
        $postData = json_encode($data);

        $ch = curl_init($this->router->generate('login_check', [], RouterInterface::ABSOLUTE_URL));
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $ch = $this->preparePostCurlOptions($ch, $postData);

        $response = curl_exec($ch);

        curl_close($ch);
        return $response;
    }


    /**
     * @param $route
     * @param $jsonToken
     * @param array $params
     * @return bool|string
     */
    public function get($route, $jsonToken, $params=[]) {

        $tokenArray = json_decode($jsonToken,1);
        $token = '';
        if(array_key_exists('token', $tokenArray)) {
            $token = $tokenArray['token'];
        }


        $ch = curl_init($this->router->generate($route, $params,RouterInterface::ABSOLUTE_URL));

        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }


    /**
     * @param $route
     * @param $token
     * @param array $params
     * @return bool|string
     */
    public function post($route, $token, $params=[]) {
        $tokenArray = json_decode($token,1);
        $request = json_encode($params);

        $ch = curl_init($this->router->generate($route, [], RouterInterface::ABSOLUTE_URL));

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
           [
                'Authorization: Bearer '.$tokenArray['token'],
                'Content-Type: application/json',
                'Content-Length: ' . strlen($request)
            ]
        );

        $ch = $this->preparePostCurlOptions($ch, $request);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * @param $route
     * @param $token
     * @param array $params
     * @return mixed
     */
    public function update($route, $token, $params = [])
    {
        $tokenArray = json_decode($token,1);

        $ch = curl_init($this->router->generate($route, $params['routeParams'], RouterInterface::ABSOLUTE_URL));
        $request = json_encode($params['entity']);


        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt_array(
            $ch, [
                CURLOPT_HTTPHEADER => [
                    'Authorization: Bearer '.$tokenArray['token'],
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($request)
                ],
                CURLOPT_CUSTOMREQUEST => "PUT",  // PUT METHOD
            ]
        );

        $ch = $this->preparePostCurlOptions($ch, $request);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * @param array $response
     * @return array|bool
     */
    protected function manageResponse($response = [], $entityClass)
    {
        if ($response === false|| !$this->isTokenValid($response)) {
            return false;
        }
        $responseArray = json_decode($response, 1);
        $results = [];
        if(isset($responseArray['hydra:member'])) {
            foreach ($responseArray['hydra:member'] as $usr) {
                $entity = new $entityClass();
                $entity->setObjectFromArray($usr);
                $results[] = $entity;
            }
        } else {
            $results[] = null;
        }

        return $results;
    }

    /**
     * @param $jsonToken
     * @return bool
     */
    protected function isTokenValid($jsonToken)
    {
        $token = json_decode($jsonToken);
        if (property_exists($token, 'code')) {
            if ($token->code == 401 || $token->code == 403) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $ch
     * @param $request
     * @return resource
     */
    protected function preparePostCurlOptions($ch, $request)
    {
        curl_setopt_array(
            $ch,
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST=> 1,
                CURLOPT_POSTFIELDS=>$request,
            ]
        );

        return $ch;
    }

    protected function generateDateString() {
        $date = new \DateTime();
        return $date->format('Y/m/d H:i:s');
    }

    /**
     * @return RouterInterface
     */
    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    /**
     * @required
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }
}