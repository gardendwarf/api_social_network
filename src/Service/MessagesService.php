<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 19/12/18
 * Time: 21:04
 */

namespace App\Service;


use App\Entity\Message;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MessagesService
 * @package App\Service
 */
class MessagesService extends ApiClient
{
    const POST_MESSAGE_ROUTE='api_messages_post_collection';
    const GET_MESSAGES_ROUTE='api_messages_get_collection';
    const GET_MESSAGE_ROUTE='api_messages_get_item';
    const PUT_MESSAGE_ROUTE='api_messages_put_item';
    const DEL_MESSAGE_ROUTE='api_messages_delete_item';

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param Request $request
     * @param $formName
     * @param array $additionalValues
     * @return array|bool
     */
    public function generateReplyPostFromForm(Request $request, $formName, array $additionalValues = [])
    {
        if(!$request->request->get($formName)) {
            return false;
        }

        $post = $request->request->get($formName);
        $post['createdAt'] = $this->generateDateString();
        $post['updatedAt'] = $this->generateDateString();
        $parent = $post['parentMessage'];
        $post['parentMessage'] = '/api/messages/'.$parent;

        if (isset($post['toUser'])) {
            $post['toUser'] = $this->router->generate(UserService::GET_USER_ROUTE, ['id' => $post['toUser']]);
        }
        if (isset($post['owner'])) {
            $post['owner'] = $this->router->generate(UserService::GET_USER_ROUTE, ['id' => $post['owner']]);
        }

        return array_merge($post, $additionalValues);
    }

    public function prepareMessagePostFromForm(Request $request, $formName, array $additionalValues = [])
    {
        if(!$request->request->get($formName)) {
            return false;
        }

        $post = $request->request->get($formName);
        $post['createdAt'] = $this->generateDateString();
        $post['updatedAt'] = $this->generateDateString();

        if (isset($post['toUser'])) {
            $post['toUser'] = $this->router->generate(UserService::GET_USER_ROUTE, ['id' => $post['toUser']]);
        }

        return array_merge($post, $additionalValues);
    }

    public function getAllMyMessages(SessionInterface $session, $params=[])
    {
        $jwtToken = $session->get('token');

        $response = json_decode($this->get(self::GET_MESSAGES_ROUTE, $jwtToken, $params), 1);

        $messages = [];
        if(isset($response["hydra:member"])) {
            foreach ($response["hydra:member"] as $item) {
                $message = new Message();
                $message->setObjectFromArray($item);
                $messages[] = $message;
            }
        }

        return $messages;
    }

    /**
     * @param SessionInterface $session
     * @param $params
     * @return array|bool
     */
    public function createMessage(SessionInterface $session, $params)
    {
        return $this->manageResponse(
            $this->post(self::POST_MESSAGE_ROUTE, $session->get('token'), $params),
            Message::class
        );
    }


    /**
     * @param FormFactory $formFactory
     * @param array $context
     * @return FormInterface
     */
    public function createMessageForm(FormFactory $formFactory, $context = [])
    {
        $formName = ($context['formName']) ?? 'form';
        $message = new Message();
        $messageForm = $formFactory->createNamedBuilder($formName, FormType::class, $message)
            ->add('toUser', EntityType::class, ['class' => User::class, 'choice_label' => 'completeName', 'placeholder' => 'Choose an user'])
            ->add('subject', TextType::class)
            ->add('bodyMessage', TextareaType::class)
            ->add('save', SubmitType::class, ['label' => 'send message', 'attr' => ['class' => 'btn btn-success']])
            ->getForm();

        if (isset($context['children']) && sizeof($context['children']) > 0) {
            $children = $context['children'];
            foreach ($children as $child) {
                $messageForm->add($child['property'], $child['FormType'], $child['options']);
            }
        }

        return $messageForm;
    }

}