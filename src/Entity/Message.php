<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use App\Filter\OrSearchFilter;
use App\Tools\CastableToArray;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(normalizationContext={"groups"={"message"}}, attributes={"order"={"updatedAt": "DESC"}})
 * @ApiFilter(DateFilter::class, properties={"updatedAt"})
 * @ApiFilter(
 *     OrSearchFilter::class, properties={
 *         "my_messages": {"owner.id", "toUser.id"}
 *     }
 * )
 * @ApiFilter(ExistsFilter::class, properties={"parentMessage"})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "subject":"partial"})
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    use CastableToArray;
    /**
     * @ORM\Id()
     * @Groups({"message"})
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"message"})
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * @Groups({"message"})
     * @ORM\Column(type="text")
     */
    private $bodyMessage;

    /**
     * @Groups({"message"})
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @Groups({"message"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Message", inversedBy="replies")
     */
    private $parentMessage;

    /**
     * @Groups({"message"})
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="parentMessage")
     */
    private $replies;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Groups({"message"})
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @Groups({"message"})
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     *
     * @Groups({"message"})
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="receivedMessages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $toUser;

    public function __construct()
    {
        $this->replies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getBodyMessage(): ?string
    {
        return $this->bodyMessage;
    }

    public function setBodyMessage(string $bodyMessage): self
    {
        $this->bodyMessage = $bodyMessage;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getParentMessage(): ?self
    {
        return $this->parentMessage;
    }

    public function setParentMessage(?self $parentMessage): self
    {
        $this->parentMessage = $parentMessage;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReplies()
    {
        return $this->replies;
    }

    public function addReply(self $reply): self
    {
        if (!$this->replies->contains($reply)) {
            $this->replies[] = $reply;
            $reply->setParentMessage($this);
        }

        return $this;
    }

    public function removeReply(self $reply): self
    {
        if ($this->replies->contains($reply)) {
            $this->replies->removeElement($reply);
            // set the owning side to null (unless already changed)
            if ($reply->getParentMessage() === $this) {
                $reply->setParentMessage(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function setObjectFromArray($array = array())
    {
        if (!is_null($array)) {
            foreach ($array as $property => $value) {
                if ($value) {
                    if (property_exists(get_class($this), $property)) {
                        if($property == 'parentMessage' && is_array($value)) {
                            $message = new Message();
                            $message->setObjectFromArray($value);
                            $this->addReply($message);
                        } elseif ( $property == 'replies') {
                            foreach ($value as $item) {
                                $reply = new Message();
                                $reply->setObjectFromArray($item);
                                $this->addReply($reply);
                            }
                        } elseif ($property == 'owner') {
                            $owner = new User();
                            $owner->setObjectFromArray($value);
                            $this->setOwner($owner);
                        }elseif ($property == "toUser")  {
                            $toUser = new User();
                            $toUser->setObjectFromArray($value);
                            $this->setToUser($toUser);
                        } elseif($property == 'createdAt' || $property == 'updatedAt' || $property == 'deletedAt') {
                            if($value) {
                                $this->$property = new \DateTime($value);
                            }
                        } else {
                            $this->$property = $value;
                        }
                    }
                }
            }
        }
    }

    public function getToUser(): ?User
    {
        return $this->toUser;
    }

    public function setToUser(?User $toUser): self
    {
        $this->toUser = $toUser;

        return $this;
    }

}

