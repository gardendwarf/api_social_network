<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use App\Tools\CastableToArray;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ApiResource(normalizationContext={"groups"={"user", "friendship"}, "enable_max_depth"=true})
 * @ORM\Entity(repositoryClass="App\Repository\FriendshipRepository")
 * @ApiFilter(DateFilter::class, properties={"updatedAt"})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "user": "exact", "connection":"exact"})
 */
class Friendship
{
    use CastableToArray;

    /**
     * @Groups({"user", "friendship"})
     * @MaxDepth(2)
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friends")
     * @ORM\Id
     */
    private $user;

    /**
     * @Groups({"user", "friendship"})
     * @MaxDepth(2)
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="friendsWithMe")
     * @ORM\Id
     */
    private $friend;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAccepted;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFriend(): ?User
    {
        return $this->friend;
    }

    public function setFriend(?User $friend): self
    {
        $this->friend = $friend;

        return $this;
    }

    public function getIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(bool $isAccepted): self
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    public function setObjectFromArray($array = array())
    {
        if (!is_null($array)) {
            foreach ($array as $property => $value) {
                if ($value) {
                    if (property_exists(get_class($this), $property)) {
                        if($property == 'friend' && is_array($value)) {
                            $friend = new User();
                            $friend->setObjectFromArray($value);
                            $this->friend = $friend;
                        } else if($property == 'user' && is_array($value)) {
                            $user = new User();
                            $user->setObjectFromArray($value);
                            $this->user = $user;
                        } else {
                            $this->$property = $value;
                        }
                    }
                }
            }
        }
    }
}
