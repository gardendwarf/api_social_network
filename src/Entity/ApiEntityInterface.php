<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 22/12/18
 * Time: 21:49
 */

namespace App\Entity;


interface ApiEntityInterface
{

    /**
     * @param array $array
     * @return mixeds
     */
    public function setObjectFromArray(array $array = []);
}