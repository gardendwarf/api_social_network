<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/12/18
 * Time: 16:39
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AuthenticationController
 * @package App\Controller
 */
class AuthenticationController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $name = $request->request->get('name');
        $firstname = $request->request->get('firstName');
        $createdAt = $request->request->get('createdAt');
        $updatedAt = $request->request->get('updatedAt');

        $user = new User();
        $user->setUsername($username)
            ->setPassword($encoder->encodePassword($user, $password))
            ->setName($name)
            ->setFirstName($firstname)
            ->setCreatedAt(new \DateTime($createdAt))
            ->setUpdatedAt(new \DateTime($updatedAt))
            ->setIsActive(true);
        $em->persist($user);
        $em->flush();
        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

}