<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 17/12/18
 * Time: 12:12
 */

namespace App\Controller;


use App\Entity\Friendship;
use App\Entity\Message;
use App\Entity\User;
use App\Service\ApiClient;
use App\Service\FriendshipService;
use App\Service\MessagesService;
use App\Service\UserService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\RouterInterface;


class ClientController extends AbstractController
{
    const API_URL='http://localhost/';
    const REPLY_FORM_NAME = 'reply_form';
    const FRIENDSHIP_FORM_NAME = 'friend_form';
    const NEW = 1;
    const EDIT = 2;
    const DEL = 3;

    /**
     * @var SessionInterface
     */
    protected $session;
    /**
     * @var RouterInterface
     */
    protected $router;
    /**
     * @var UserService
     */
    protected $userService;
    /**
     * @var MessagesService
     */
    protected $messageService;
    /**
     * @var FriendshipService
     */
    protected $friendshipService;


    /**
     * @Route("/home/{id}", defaults={"id"=0}, name="index")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index($id, Request $request)
    {
        // tableau des paramètres passés au twig
        $twigParams = [];
        // Si le token est invalide ou si il est absent --> fallback to login
        if(!$this->hasTokenInSession() || !$this->isUserLoggedIn()) {
            return $this->redirect($this->generateUrl('login'));
        }

        // On récupère le user connecté
        /** @var User $currentUser */
        $currentUser = $this->userService->getUser($this->session, ['username' => $this->session->get('username')]);
        if (!$currentUser instanceof User) {
            return $this->redirect($this->generateUrl('login'));
        }
        $twigParams['currentUser'] = $currentUser;

        // Si on passe un ID c'est qu'on tente d'accéder à la timeline d'un ami...
        if($id) {
            $twigParams = array_merge(
                $twigParams,
                $this->getFriendTimeline($id, $currentUser)
            );
        } else { // Sinon on affiche celle de l'utilisateur connecté
            // Récupération des messages reçus et envoyés :
            $twigParams['myMessages'] = $this->messageService->getAllMyMessages(
                $this->session,
                ['parentMessage[exists]' => 0, 'orSearch_my_messages' => $currentUser->getId()]
            );
            $twigParams['timelineOwner'] = $currentUser;
        }
        // On récupère les amis de l'utilisateur connecté
        $myFriends = $this->userService->mergeAllMyFriends($currentUser);
        $myUsernameFriends = [];
        // On créé un table avec le username des amis pour l'affichage
        foreach ($myFriends as $myFriend) {
            $myUsernameFriends[] = $myFriend->getUsername();
        }
        $twigParams['myFriends'] = $myFriends;
        $twigParams['usernameFriends'] = $myUsernameFriends;

        // On génère le Formulaire de création de message
        $messageForm = $this->messageService->createMessageForm(
            ['formName' => 'form_messages']
        );

        if($this->manageMessagesForm($messageForm, $request, $currentUser)) {
            return $this->redirect($this->generateUrl('index'));
        }

        $editMEssageForm = $this->messageService->createMessageForm(
            ['formName' => 'form_edit_message']
        );
        $editMEssageForm->add('id', HiddenType::class)
            ->add('owner', HiddenType::class);
        if ($this->manageMessagesForm($editMEssageForm, $request, $currentUser, self::EDIT)) {
            return $this->redirect($this->generateUrl('index'));
        }

        // Others forms
        if(sizeof($request->request->all()) > 0) {
            // On gère les formulaire de reply / et de demande d'amis
            if ($route = $this->manageFormSubmission($request, $this->session, $this->messageService, $this->friendshipService)) {
                return $this->redirect($this->generateUrl($route));
            }
        }
        // on récupère la liste des utilisateurs
        $userList = $this->userService->getUserList($this->session, []);
        $twigParams['newMessageForm'] = $messageForm->createView();
        $twigParams['editMessageForm'] = $editMEssageForm->createView();
        $twigParams['userList'] = $userList;

        return $this->render(
            'client/index.html.twig',
            $twigParams
        );
    }

    /**
     * @param FormInterface $msgForm
     * @param Request $request
     * @param User $currentUser
     * @param string $type
     * @return bool
     */
    protected function manageMessagesForm(FormInterface $msgForm, Request $request, User $currentUser, string $type = self::NEW)
    {
        //die(var_dump($request->request->all()));
        $idMessage = $ownerId = null;
        if ($msgForm->getName() == 'form_edit_message') {
            $form = $request->request->get('form_edit_message');
            $idMessage = $form['id'];
            $ownerId = $form['owner'];
            unset($form['id']);
            unset($form['owner']);
            $request->request->set('form_edit_message', $form);
        }
        $msgForm->handleRequest($request);
        if ($msgForm->isSubmitted() && $msgForm->isValid()) {
            switch ($type) {
                case self::NEW:
                    $postMessage = $this->messageService->prepareMessagePostFromForm(
                        $request,
                        'form_messages',
                        ['owner' => $this->generateUrl(UserService::GET_USER_ROUTE, ['id' => $currentUser->getId()])]
                    );

                    $this->messageService->createMessage($this->session, $postMessage);
                    return true;
                case self::EDIT:
                    $postMessage = $this->messageService->prepareMessagePutFromForm(
                        $request,
                        'form_edit_message',
                        $this->session,
                        [
                            'id' => $idMessage,
                            'ownerId' => $currentUser->getId(),
                            'owner' => $this->generateUrl(UserService::GET_USER_ROUTE, ['id' => $ownerId])
                        ]
                    );

                    $this->messageService->update(
                        MessagesService::PUT_MESSAGE_ROUTE,
                        $this->session->get('token'),
                        ['routeParams' => ['id' => $idMessage], 'entity' => $postMessage]
                    );
                    return true;
                case self::DEL:
                default :
                    return false;
            }

        }
    }

    /**
     * @param $id
     * @param $currentUser
     * @return array
     */
    protected function getFriendTimeline($id, $currentUser)
    {
        $friend = $this->userService->getUser($this->session, ['id'=> $id]);
        if (!$friend instanceof User) {
            $this->get('logger')->info("Mauvais id d'utilisateur pour l'accès à latimeline - L'utilisateur avec l'id $id n'existe pas...");
            throw new HttpException(404);
        }

        if (!$this->friendshipService->isFriendship($this->session, $currentUser, $friend)) {
            $this->get('logger')->info('Utilisateurs '.$friend->getId() . ' et '.$currentUser->getId() .' ne sont pas amis.');
            throw new HttpException(403);
        }
        // Récupération des messages reçus et envoyés :
        $messageList = $this->messageService->getAllMyMessages(
            $this->session,
            ['parentMessage[exists]' => 0, 'orSearch_my_messages' => $friend->getId()]
        );
        $currentUser = $this->userService->getUser($this->session, ['username' => $this->session->get('username')]);
        if (!$currentUser instanceof User) {
            $this->get('logger')->info('Utilisateur inconnu... 403 Forbidden.');
            throw new HttpException(403);
        }

        return ['myMessages' => $messageList, 'timelineOwner' => $friend];
    }



    /**
     * @Route("/login", name="login")
     * @Route("/", name="home")
     *
     * @param Request $request
     * @param ApiClient $apiClient
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request, ApiClient $apiClient)
    {
        $user = new User();
        $form = $this->createFormBuilder($user)
            ->add('username', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, array('label' => 'Login', 'attr' => ['class' => 'btn btn-success btn-link']))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $token = $apiClient->getToken(['username' => $user->getUsername(), 'password' => $user->getPassword()]);
            if($this->isTokenValid($token)) {
                $this->get('session')->set('token', $token);
                $this->get('session')->set('username', $user->getUsername());
                return $this->redirect($this->generateUrl('index'));
            }
        }

        return $this->render('client/login.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/logout", name="logout")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout()
    {
        $this->session->clear();
        return $this->redirect($this->generateUrl('login'));
    }

    /**
     * @Route("/subscribe", name="subscribe")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function subscribe(Request $request)
    {
        $user = new User();
        $form = $this->createFormBuilder($user)
            ->add('username', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('name', TextType::class)
            ->add('firstname', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create User', 'attr' => ['class' => 'btn btn-success btn-link']))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            if($user instanceof User) {
                $response = $this->userService->subscribeUser($this->session, $user->toArray());
                if (stristr($response, 'successfully created') !== false) {
                    return $this->redirect($this->generateUrl('index'));
                }
            }

        }

        return $this->render('client/subscribe.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @param Request $request
     * @return bool|string
     */
    protected function manageFormSubmission(Request $request)
    {
        $postDatas = $request->request->all();
        $return = false;
        switch (true) {
            case isset($postDatas[self::REPLY_FORM_NAME]):
                $postDatas = $this->messageService->generateReplyPostFromForm($request, self::REPLY_FORM_NAME);
                $response = $this->messageService->createMessage($this->session, $postDatas);
               // die(var_dump($response));
                $return = 'index';
                break;
            case isset($postDatas[self::FRIENDSHIP_FORM_NAME]):
                $postValues = $this->friendshipService->createFriendshipFromForm(
                    $request,
                    self::FRIENDSHIP_FORM_NAME,
                    ['isAccepted' => true]
                );
                $this->friendshipService->createFriendship($this->session, $postValues);
                $return = 'index';
                break;
            default:
                $return = false;
        }

        return $return;
    }



    /**
     * @return bool
     */
    private function hasTokenInSession()
    {
        return !$this->session->get('token') ? false : true;
    }
    /**
     * @param $jsonToken
     * @return bool
     */
    private function isTokenValid($jsonToken)
    {
        $token = json_decode($jsonToken);

        if ($token && property_exists($token, 'code')) {
            if ($token->code == 401 || $token->code == 403) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function isUserLoggedIn()
    {
        return !$this->get('session')->get('username') ? false : true;
    }

    /**
     * @return SessionInterface
     */
    public function getSession(): SessionInterface
    {
        return $this->session;
    }

    /**
     * @required
     * @param SessionInterface $session
     */
    public function setSession(SessionInterface $session): void
    {
        $this->session = $session;
    }

    /**
     * @return RouterInterface
     */
    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    /**
     * @required
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }

    /**
     * @return UserService
     */
    public function getUserService(): UserService
    {
        return $this->userService;
    }

    /**
     * @required
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * @return MessagesService
     */
    public function getMessageService(): MessagesService
    {
        return $this->messageService;
    }

    /**
     * @required
     * @param MessagesService $messageService
     */
    public function setMessageService(MessagesService $messageService): void
    {
        $this->messageService = $messageService;
    }

    /**
     * @return FriendshipService
     */
    public function getFriendshipService(): FriendshipService
    {
        return $this->friendshipService;
    }

    /**
     * @required
     * @param FriendshipService $friendshipService
     */
    public function setFriendshipService(FriendshipService $friendshipService): void
    {
        $this->friendshipService = $friendshipService;
    }


}