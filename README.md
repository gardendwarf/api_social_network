# Réseau Social avec API Sécurisée

## Présentation

## Prérequis

Ce projet nécessite un serveur WEB de type Apache 2.2/2.4 PHP 7.2, et un serveur de SGDR MySQL/MariaDB
Les sources de configuration du projet en local sont toutes présentes dans le dossier resources

## Installation et configuration

### Récupération des sources
lancer la commande suivante :

 `git clone https://gitlab.com/gardendwarf/api_social_network.git`

Cela vous a créé un répertoire api_social_network dans lequel vous devez vous déplacer afin de réaliser les commandes de configuration suivantes.

### Configuration du projet

Dans un premier temps, il faut copier et renommer le fichier .env.dist situé à la racine du projet.
Une fois cette étape réalisée, il faut ensuite le paramétrer selon vos critères locaux (surtout pour la BDD)


### Base de données

La base de données peut être générée par doctrine si besoin, mais dans tous les cas de figure (notamment le cas ou l'utilisateur doctrine n'a pas les droits de création de schémas), il existe dans le dossier resources/sql un fichier permettant de créer le schéma de BDD ainsi que les tables.

### Composer / yarn

L'installation des vendors du projet s'effectue de manière assez simple en lançant la commande depuis la racine du projet :

`composer install`

Si vous n'avez pas créé de fichier .env, comme précisé à l'étape précédente, vous risquez de rencontrer cette erreur :

`Executing script cache:clear [KO]
 [KO]
Script cache:clear returned with error code 255
!!  PHP Fatal error:  Uncaught Symfony\Component\Dotenv\Exception\PathException: Unable to read the ".../.env" environment file. in .../vendor/symfony/dotenv/Dotenv.php:466
!!  Stack trace:
!!  #0 .../vendor/symfony/dotenv/Dotenv.php(51): Symfony\Component\Dotenv\Dotenv->doLoad(false, Array)
!!  #1 .../vendor/symfony/dotenv/Dotenv.php(71): Symfony\Component\Dotenv\Dotenv->load('...')
!!  #2 .../config/bootstrap.php(16): Symfony\Component\Dotenv\Dotenv->loadEnv('...')
!!  #3 .../bin/console(28): require('...')
!!  #4 {main}
!!    thrown in .../vendor/symfony/dotenv/Dotenv.php on line 466
!!  
Script @auto-scripts was called via post-install-cmd`


Une fois l'ensemble des packages installés, il faudra encore lancer cette commande afin d'installer toutes les dépendences du projet pour les assets :

`yarn install`

Comme précisé plus haut, yarn est nécessaire pour la compilation des assets. pour ce faire vous pouvez lancer la commande :
* En environnement de production:

`yarn encore production`

* En environnement de développement:

`yarn encore --dev`

Voilà la configuration initiale du projet est terminée.